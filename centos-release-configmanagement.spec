Name:           centos-release-configmanagement
Version:        1
Release:        2%{?dist}
Summary:        Release file for ConfigManagement SIG repository

License:        GPL
URL:            http://wiki.centos.org/SpecialInterestGroup/ConfigManagementSIG
Source0:        RPM-GPG-KEY-CentOS-SIG-ConfigManagement

BuildArch:      noarch
Epoch:		666

Provides:       centos-release-configmanagement = %{version}-%{release}

%description
Provides config files for the ConfigManagement SIG

%prep

%build

%install
mkdir -p %{buildroot}%{_sysconfdir}/pki/rpm-gpg
install -m 644 %SOURCE0 %{buildroot}%{_sysconfdir}/pki/rpm-gpg/

%files
%defattr(-,root,root)
%{_sysconfdir}/pki/rpm-gpg/RPM-GPG-KEY-CentOS-SIG-ConfigManagement

%changelog
* Fri Apr 03 2020 Ben Morrice <ben.morrice@cern.ch> - 1-2
- Update for CERN (linuxsoft.cern.ch)

* Tue Mar 31 2020 Fabian Arrotin <arrfab@centos.org> - 1-1
- Initial build
